﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lab1.Contract;
using Lab1.Implementation;

namespace Lab1.Main
{
    public class Program
    {
        static void Main(string[] args)
        {
            var kolekcja = CollectionFactoryMethod();
            CollectionConsumerMethod(kolekcja);

            Console.ReadKey();
        }

        public static IEnumerable<IMałpa> CollectionFactoryMethod()
        {
            var implementacja_1 = new Impl1();
            var implementacja_2 = new Impl2();

            return new List<IMałpa> { implementacja_1, implementacja_2 };
        }

        public static void CollectionConsumerMethod(IEnumerable<IMałpa> kolekcja)
        {
            foreach (IMałpa implementacja in kolekcja)
            {
                implementacja.Metoda1();
            }
        }
    }
}
